<?php 
require_once 'Unpacker.php';
require_once 'bzUnpack.php';

class bzUnpacker extends Unpacker
{
	public function getUnpacker()
	{
		return new bzUnpack($this->srcDir, $this->destDir);
	}
}