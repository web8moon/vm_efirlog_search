<?php 
class bzUnpack
{
	protected $srcDir, $destDir;
	
	public function __construct(string $srcDir = '', string $destDir = '')
	{
		$this->srcDir = $srcDir;
		$this->destDir = $destDir;
	}
	
	
	public function run(string $fileName): string
	{
		$newFile = $this->getNewFileName($fileName);
		if (!empty($this->srcDir)) {
			$fileName = $this->srcDir . DIRECTORY_SEPARATOR . $fileName;
		}
		$f = fopen($newFile, 'x');
	    $bz = bzopen($fileName, 'r');

        while (!feof($bz)) {
			$uncompressed = bzread($bz, 4096);
			if ($uncompressed !== false) {
				fwrite($f, $uncompressed);
			}
        }
        bzclose($bz);
		fclose($f);

        return $newFile;
	}
	
	
	protected function getNewFileName(string $fileName): string
	{
		if (stripos($fileName, '.tbz2') !== false) {
            $newFile = str_replace('.tbz2', '.tar', $fileName);
        } else {
            if (stripos($fileName, '.tbz') !== false) {
                $newFile = str_replace('.tbz', '.tar', $fileName);
            } else {
                $newFile = str_replace('.bz2', '', $fileName);
            }
        }

		if (!empty($this->destDir)) {
			$newFile = $this->destDir . DIRECTORY_SEPARATOR . $newFile;
		}
		
		return $newFile;
	}
}