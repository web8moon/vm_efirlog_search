<?php
require_once 'FileIterator.php';

class Searcher
{
	private $search;
	
	public function __construct(string $search = '')
	{
		$this->search = $search;
	}
	

	public function search(string $fileName)
	{
		$position = false;
		$fileIterator = new FileIterator($fileName);
		foreach($fileIterator as $line) {
			$position = stripos($line, $this->search);
			if ($position !== false) {
				break;
			}
		}
		
		return $position;
	}
}