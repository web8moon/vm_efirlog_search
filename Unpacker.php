<?php 
abstract class Unpacker {

	protected $srcDir, $destDir;
	
	public function __construct(string $srcDir = '', string $destDir = '')
	{
		$this->srcDir = $srcDir;
		$this->destDir = $destDir;
	}
	
	abstract public function getUnpacker();
	
	
	public function unpack(string $fileName): string
	{
		$unpacker = $this->getUnpacker();
		$newFile = $unpacker->run($fileName);
		
		return $newFile;
	}
}