<?php 
declare(strict_types = 1);
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');

require_once 'bzUnpacker.php';
require_once 'Searcher.php';

$srcDir = 'efir_one_c_logs';
$destDir = 'found';

foreach(scandir($destDir) as $file) {
	if (!is_dir($file)) {
		echo 'Emptying destination folder "' . $destDir . '"' . PHP_EOL;
		unlink($file);
	}
}

$searcher = new Searcher('2061775');
$unpacker = new bzUnpacker($srcDir, $destDir);

foreach(scandir($srcDir) as $file) {
	if (is_dir($file)) {
		continue;
	}
	
	echo 'File ' . $file . ' ... ';
	
	$newFile = $unpacker->unpack($file);
	echo ' searching ... ';
	$result = $searcher->search($newFile);
	if ($result === false) {
		echo 'not found' . PHP_EOL;
		unlink($newFile);
	} else {
		echo 'found at position ' . $result . PHP_EOL;
	}
}